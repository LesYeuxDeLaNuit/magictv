/* Magic Mirror Config Sample *
 * By Michael Teeuw http://michaelteeuw.nl
 * MIT Licensed.
 *
 * For more information how you can configurate this file
 * See https://github.com/MichMich/MagicMirror#configuration
 *
 */

var config = {
	address: "", // Address to listen on, can be:
	                      // - "localhost", "127.0.0.1", "::1" to listen on loopback interface
	                      // - another specific IPv4/6 to listen on a specific interface
	                      // - "", "0.0.0.0", "::" to listen on any interface
	                      // Default, when address config is left out, is "localhost"
	port: 8080,
	ipWhitelist: [], // Set [] to allow all IP addresses
	                                                       // or add a specific IPv4 of 192.168.1.5 :
	                                                       // ["127.0.0.1", "::ffff:127.0.0.1", "::1", "::ffff:192.168.1.5"],
	                                                       // or IPv4 range of 192.168.3.0 --> 192.168.3.15 use CIDR format :
	                                                       // ["127.0.0.1", "::ffff:127.0.0.1", "::1", "::ffff:192.168.3.0/28"],

	language: "fr",
	timeFormat: 24,
	units: "metric",

	modules: [
		{
			module: "alert",
		},
		{
			module: "clock",
			position: "top_left"
		},
		 {
	                module: "currentweather",
        	        position: "top_right",  // This can be any of the regions.
                                                                        // Best results in left or right regions.
              		  config: {
                       		 // See 'Configuration options' for more information.
                      		 location: "Perpignan,France",
                    		 locationID: "2987914", //Location ID from http://openweathermap.org/help/city_list.txt
                    		 appid: "29ea637942a3e6847e6fba1eAeefffe5" //openweathermap.org API key.
        	        }
	        },


		{
			module: "helloworld",
			position: "bottom_bar",	// This can be any of the regions.
			config: {
				// See 'Configuration options' for more information.
				text: "Il n'a pas de plaisir sans labeur"
				}
		},
		{
			module: "newsfeed",
			position: "bottom_bar",
			config: {
				feeds: [
					{
						title: "Des news de Lemonde",
						url: "https://www.lemonde.fr/rss/une.xml",
					}
				],
				showSourceTitle: true,
				showPublishDate: true,
			}
		},
	]

};

/*************** DO NOT EDIT THE LINE BELOW ***************/
if (typeof module !== "undefined") {module.exports = config;}
