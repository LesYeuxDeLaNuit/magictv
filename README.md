# MagicTv
---
## Initialisation de la Raspberry
  * Télécharger la dernière version de Raspbian
  * Décompressez l’archive
  * Téléchargez Win32DiskImager
  * Insérez la carte SD dans l’ordinateur, formater la et lancez Win32DiskImager
  * Flasher la carte SD avec Raspbian
---

## Configurer Raspbian
  * Pour passer votre clavier en AZERTY sur Raspbian, tapez `sudo raspi-config` dans le terminal, puis dans les menus, rendez-vous dans : « Localisation Options » / « Change keyboard layout », choisissez « Français » avec une disposition « Par défaut » et validez.
  * Pour activer le SSH sur Raspbian, tapez `sudo raspi-config` dans le terminal, puis dans les menus, rendez-vous dans : « Configurer les connexions des périphériques » / « SSH », activez et validez.
  * Connectez le Raspberry au réseau.
  * Puis mettre le Raspberry à jour avec 'sudo apt-get update' et 'sudo apt-get upgrade'
  * Reboot le Raspberry `sudo reboot`
---

## Connection SSH
  * Sur le terminal du Raspberry trouvez son ip avec `ifconfig`
  * Sur un ordinateur lancez PuTTY et connectez-vous en ssh sur la Raspberry  	*Identifiant : pi Mot de passe : raspberry* 
---

## Installation de [MagicMirror](https://github.com/MichMich/MagicMirror)
  * Installation de npm `sudo apt-get install npm`
  * Installation de Node.js `curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash –` et `sudo apt install -y nodejs`
  * Clone du repository de MagicMirror `git clone https://github.com/MichMich/MagicMirror`
  * Taper `cd MagicMirror/`
  * Allez dans le dossier MagicMirror et lancez l’installation de MagicMirror `npm install`
  * Lancer MagicMirror `npm start` (Ctrl + c pour stopper)
---

## Ajouts des fonctionnalités
  * Remplacer dans la raspberry les fichiers dans le répertoire MagicMirror par les fichiers fournis sur clef USB.
---

## The End
  * Dans `cd MagicMirror/` tapez `npm start` (Ctrl + c pour stopper)